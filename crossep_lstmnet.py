import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import logging
from profiler import Profiler, AvgCounter
from network import Network
import initializer
import optimizer

np.random.seed(7)


class LSTMNetwork(Network):

    def __init__(self, config, datagen=None):
        super(LSTMNetwork, self).__init__(config, datagen)
        self.s_vocab = config["s_vocab"]
        self.s_embed = config["s_embed"]
        self.s_window = config["s_window"]
        self.s_hidden = config["s_hidden"]
        self.batch_size = config["batch_size"]
        self.s_output = 4

        self.p_lambda = config.get("p_lambda", 0.0001)
        self.p_mloss = config.get("p_mloss", 0.2)
        self.p_rho = self.config.get("p_rho", 0.9)
        self.p_lr = self.config.get("p_lr", 2.0)
        self.p_eps = self.config.get("p_eps", 0)
        self.p_optimizer = self.config.get("p_optimizer", "clipped_adagrad")
        self.is_train = self.config.get("is_train", True)

        self.build_network_wrapper()

    def build_input(self):
        self.input["x_in"] = T.itensor3('x_in')
        self.input["yp_in"] = T.imatrix('yp_in')
        self.input["yg_in"] = T.imatrix('yg_in')
        self.input["mask_in"] = T.imatrix('mask_in')


    def build_wb(self, wb_shape):
        for wb_s_key in wb_shape.keys():
            self.wb[wb_s_key] = initializer.weight_init(wb_shape[wb_s_key])
        temp_shape = self.wb["w_y"].get_value().shape
        self.wb["w_y"].set_value((np.zeros(temp_shape)).astype(theano.config.floatX))

    def core_lstm(self, x_in, h_tm1, c_tm1):
        x_in_1dim = T.reshape(x_in, newshape=(self.batch_size * self.s_window,))
        emb_lookup = self.wb["w_embed"][x_in_1dim]
        x_t = T.reshape(emb_lookup, newshape=(self.batch_size, self.s_embed * self.s_window))


        i_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ix"]) +
                             T.dot(c_tm1, self.wb["w_ic"]) + T.dot(h_tm1, self.wb["w_ih"]) + self.wb["b_i"])

        f_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_fx"]) +
                             T.dot(c_tm1, self.wb["w_fc"]) + T.dot(h_tm1, self.wb["w_fh"]) + self.wb["b_f"])

        x_cin = T.tanh(T.dot(x_t, self.wb["w_cx"]) + T.dot(h_tm1, self.wb["w_ch"]) + self.wb["b_c"])

        c_t = T.mul(f_t, c_tm1) + T.mul(i_t, x_cin)

        o_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ox"]) +
                             T.dot(c_t, self.wb["w_oc"]) + T.dot(h_tm1, self.wb["w_oh"]) + self.wb["b_o"])
        h_t = T.mul(o_t, T.tanh(c_t))

        y_result = T.nnet.softmax(T.dot(h_t, self.wb["w_out"]) + self.wb["b_out"])

        return h_t, c_t, y_result


    def scan_builder(self):
        h_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))
        c_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))

        yresult_scan, _ = theano.scan(
            self.core_lstm,
            sequences=[self.input["x_in"]],
            outputs_info=[h_0, c_0, None]
        )

        return yresult_scan

    def build_networks(self, wb_shape, scan_builder):
        self.build_input()
        self.build_wb(wb_shape)
        yresult_scan = scan_builder()
        self.func_yresult = theano.function(
            inputs=[self.input["x_in"]],
            outputs=yresult_scan,
            allow_input_downcast=True
        )



    def build_trainer(self, trainer_scan):

        s_seq_pred = None

        s_seq_gold = None


        mask_avg = T.sum(T.cast(self.input["mask_in"], theano.config.floatX), axis=0)

        mismatch = T.sum(T.cast(T.neq(self.input["yp_in"], self.input["yg_in"]), theano.config.floatX),
                         axis=0)

        delta_sum = self.p_mloss * mismatch

        error = (T.sum(mismatch) / (self.input["yp_in"].shape[0]*self.input["yp_in"].shape[1]))

        s_seq_pred_sum = T.sum(s_seq_pred, axis=0)

        s_seq_gold_sum = T.sum(s_seq_gold, axis=0)

        reg_cost = optimizer.l2_regularizer(self.wb)

        error_cost = T.sum(T.maximum(0, s_seq_pred_sum + delta_sum - s_seq_gold_sum) / mask_avg) / self.batch_size

        cost = error_cost + self.p_lambda * reg_cost

        gd = OrderedDict()
        for wb_key in self.wb.keys():
            gd[wb_key] = T.grad(cost=cost, wrt=self.wb[wb_key])

        if self.p_optimizer == "adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)
        elif self.p_optimizer == "clipped_adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.clipped_adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)
        elif self.p_optimizer =="adadelta":
            wb_updates, gradients_sq, deltas_sq = optimizer.adadelta_updates(self.wb, gd.values(), self.p_rho, self.p_eps)
        else:
            wb_updates, gradients_sq, deltas_sq = optimizer.sgd_update(self.wb, gd.values(), self.p_lr)

        self.gradients_sq = gradients_sq
        self.deltas_sq = deltas_sq

        self.func_trainer = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            updates=wb_updates,
            allow_input_downcast=True
        )

        self.func_validator = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            allow_input_downcast=True
        )


    def build_network_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),

            "w_cx": (self.s_embed * self.s_window, self.s_hidden),
            "w_ch": (self.s_hidden, self.s_hidden),
            "b_c": (self.s_hidden,),

            "w_ix": (self.s_embed * self.s_window, self.s_hidden),
            "w_ih": (self.s_hidden, self.s_hidden),
            "w_ic": (self.s_hidden, self.s_hidden),
            "b_i": (self.s_hidden,),

            "w_fx": (self.s_embed * self.s_window, self.s_hidden),
            "w_fh": (self.s_hidden, self.s_hidden),
            "w_fc": (self.s_hidden, self.s_hidden),
            "b_f": (self.s_hidden,),

            "w_ox": (self.s_embed * self.s_window, self.s_hidden),
            "w_oh": (self.s_hidden, self.s_hidden),
            "w_oc": (self.s_hidden, self.s_hidden),
            "b_o": (self.s_hidden,),

            "w_out": (self.s_hidden, self.s_output),
            "b_out": (self.s_output,),
            "w_y": (self.s_output, self.s_output)
        })

        self.build_networks(wb_shape=wb_shape, scan_builder=self.scan_builder)



def main():
    s_vocab = 4000
    s_window = 5
    batch_size = 20
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 100,
        "s_hidden": 150,
        "batch_size": batch_size,
        "p_lr": 0.2,
        "p_eps": 0.2,
        "p_mloss": 0.2,
        "p_lambda": 0.0001
    }

    model_name = "model_23.json"

if __name__ == "__main__":
    main()