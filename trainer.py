import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json
import logging
from profiler import Profiler, AvgCounter
import maxmgnet1
import maxmgnet_feedforward
import maxmgnet_lstm
import data_generator
import my_logger
from segmentation import run_segmentation


def get_correct_ratio(golden, predicted):
    assert len(golden) == len(predicted)
    return np.sum(golden == (predicted > 0.5).astype(int)) / float(len(golden))


class Trainer(object):
    def __init__(self, network):
        self.my_print = my_logger.my_print
        self.network = network
        # self.datagen = datagen
        pass

    def trainig(self, nconfig):
        model_name_final = ""
        logfile = nconfig.get("logfile", "out.log")
        my_logger.set_logfile(logfile=logfile)
        tprofiler = Profiler("train")
        avg_t_cost = AvgCounter("cost")
        avg_t_error = AvgCounter("error")

        # avg_v_cost_all = AvgCounter("valid cost all")
        avg_v_error_all = AvgCounter("valid error all")

        # avg_t_cost_all = AvgCounter("train cost all")
        avg_t_error_all = AvgCounter("train error all")

        avg_error_cost = AvgCounter("error cost")
        avg_reg_cost = AvgCounter("reg cost")

        model_name = nconfig.get("model_name", "model_%s.json")
        model_count = nconfig.get("model_offset", 0)
        print_time = nconfig.get("print_time", 1000)
        wait_time = nconfig.get("valid_wait_time", 30)
        train_iter = nconfig.get("iter", 400)

        min_valid_error = np.inf
        valid_wait = 0
        for iter in range(train_iter):
            tprofiler.start()
            j = 0
            # for line in read_input_file(train_file_idx, False):
            for result in self.network.run_t_func():

                j += 1

                avg_t_cost.add(result[0])
                avg_t_error.add(result[1])
                avg_t_error_all.add(result[1])
                avg_error_cost.add(result[-2])
                avg_reg_cost.add(result[-1])

                if j % print_time == 0:
                    msg = "iter=%s, j=%s, %s ,%s" % (
                        iter, j,
                        avg_t_cost.get_and_reset_str(),
                        avg_t_error.get_and_reset_str()
                    )
                    self.my_print(msg)

                    msg2 = "%s, %s" % (
                        avg_error_cost.get_and_reset_str(),
                        avg_reg_cost.get_and_reset_str()
                    )
                    self.my_print(msg2)

            self.my_print("do validation...")

            for result in self.network.run_v_func():
                # avg_v_cost_all.add(result[0])
                avg_v_error_all.add(result[1])

            # avg_v_cost_val = avg_v_cost_all.get_and_reset()
            avg_v_error_val = avg_v_error_all.get_and_reset()
            avg_t_error_val = avg_t_error_all.get_and_reset()

            msg = "avg v error : %s" % (avg_v_error_val)
            msg += ", avg t error : %s" % (avg_t_error_val)

            if avg_v_error_val < min_valid_error:
                valid_wait = 0
                min_valid_error = avg_v_error_val
                model_name_save = model_name % model_count
                model_count += 1
                self.network.write_model(model_name_save)
                msg += ", save_model=%s" % (model_name_save)
                model_name_final = model_name_save
            else:
                valid_wait += 1
                msg += ", valid_wait=%s" % (valid_wait)
                if valid_wait > wait_time:
                    msg += ", early stop"
                    self.my_print(msg)
                    break

            self.my_print(msg)
            tprofiler.stop()
            self.my_print(tprofiler.get_period())
        return model_name_final


def trainer1():
    s_vocab = 4000
    s_window = 5

    mmnetwork = maxmgnet1.MaxMgNetwork1({
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 100,
        "s_hidden": 100,
    })

    mdata_gen = data_generator.DataGenerator({
        "pad_id": s_vocab - 1,
        "s_window": s_window,
        "train_file_idx": "./data/converted/pku_t_label.txt",
        "valid_file_idx": "./data/converted/pku_v_label.txt",
    })
    mtrainer = Trainer(mmnetwork)
    mtrainer.trainig({
        "print_time": 10,
    })


def trainer2_try():
    for s_embed, s_hidden in [[25, 20], [25, 50], [50, 40], [50, 100], [100, 50], [100, 100], [100, 200]]:
        my_logger.my_print("s_embed:%s, s_hidden:%s" % (s_embed, s_hidden))
        s_vocab = 4000
        s_window = 5
        batch_size = 50
        config = {
            "pad_id": s_vocab - 1,
            "s_vocab": s_vocab,
            "s_window": s_window,
            "s_embed": s_embed,
            "s_hidden": s_hidden,
            "batch_size": batch_size,
            "train_file_idx": "./data/converted/pku_t_label.txt",
            "valid_file_idx": "./data/converted/pku_v_label.txt",
            "model_name": "model/model_e_%s_h_%s_%s.json" % (s_embed, s_hidden, "%s"),
            "print_time": 10,
        }
        mdata_gen = data_generator.BatchDataGenerator(config)
        mmnetwork = maxmgnet_feedforward.MaxMgNetworkFeedForward(config, mdata_gen)
        mtrainer = Trainer(mmnetwork)
        model_name = mtrainer.trainig(config)
        my_logger.my_print("evaluate: s_embed:%s, s_hidden:%s" % (s_embed, s_hidden))
        run_segmentation(config, model_name, maxmgnet_feedforward.MaxMgNetworkFeedForward)
        my_logger.my_print("-------------------")


if __name__ == "__main__":
    pass
