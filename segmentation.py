import theano.tensor as T
import theano
import copy
import numpy as np
from util import read_json, read_file_line, tagDigitEn, get_widx
from collections import OrderedDict
from segeval import segeval
import maxmgnet_feedforward
import maxmgnet_lstm
import my_logger
import difflib


#   U B L I
# U
# B
# L
# I
class Segmentation(object):
    def __init__(self, config):
        self.s_window = config["s_window"]
        self.pad_id = config["s_vocab"] - 1
        self.fname_in = config["fname_in"]
        self.fname_out = config["fname_out"]
        self.fname_dict = config["fname_dict"]
        self.fname_eval_dict = config["fname_eval_dict"]
        self.fname_eval_gold = config["fname_eval_gold"]
        self.my_print = my_logger.my_print
        self.word_dict = read_json(self.fname_dict)

    def gen_input_line(self, line):
        line_word = [self.pad_id] * (self.s_window / 2) + line + [self.pad_id] * (
            self.s_window / 2)
        case_raw = [line_word[i:i + self.s_window] for i in range(len(line_word) + 1 - self.s_window)]
        return case_raw

    def word_segmentation(self, network):
        fout = open(self.fname_out, "w")
        for line in read_file_line(self.fname_in, True):
            line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line.decode("utf-8")])
            line_write = []
            for w in line2:
                w_idx = get_widx(w, self.word_dict)
                line_write.append(w_idx)
            s = u""
            if len(line2) >= 1:
                input_data_raw = self.gen_input_line(line_write)
                input_data = np.expand_dims(input_data_raw, axis=1)
                tag_probs = network.y_seq_func(input_data)
                tag_result = network.viterbi_numpy(tag_probs[:, 0, :])
                for w, t in zip(line2, tag_result):
                    s += w
                    if t == 0 or t == 2:
                        s += u"  "
            s += u"\n"
            fout.write(s.encode('utf-8'))
        fout.close()

    def run_network(self, network):
        self.word_segmentation(network)
        self.segeval()

    def segeval(self):

        fg = open(self.fname_eval_gold)
        fj = open(self.fname_out)
        fd = open(self.fname_eval_dict)

        lineg = fg.readlines()
        linej = fj.readlines()
        lined = fd.readlines()
        fg.close()
        fj.close()
        fd.close()

        mdict = set([x.strip().decode("utf-8") for x in lined])
        i = 0
        count_g = 0
        count_j = 0
        count_match = 0
        iv_all = 0
        iv_correct = 0
        oov_all = 0
        oov_correct = 0

        for lines in zip(lineg, linej):
            # print i
            l0 = lines[0].decode('utf-8').strip().split("  ")
            l1 = lines[1].decode('utf-8').strip().split("  ")

            s = difflib.SequenceMatcher(None, l0, l1)
            match_block = s.get_matching_blocks()
            match_range = reduce(lambda a, b: a + b, [range(x[0], x[0] + x[2]) for x in match_block])

            c_g = len(l0)
            c_j = len(l1)
            c_m = sum(x[2] for x in match_block)
            for idx, w in enumerate(l0):
                if w in mdict:
                    iv_all += 1
                    if idx in match_range:
                        iv_correct += 1
                else:
                    oov_all += 1
                    if idx in match_range:
                        oov_correct += 1

            count_g += c_g
            count_j += c_j
            count_match += c_m
            i += 1

        m_r = count_match / float(count_g)
        m_p = count_match / float(count_j)
        m_f = 2 * m_p * m_r / (m_p + m_r)
        m_oov_r = oov_all / float(iv_all + oov_all)
        m_iv = iv_correct / float(iv_all)
        m_oov = oov_correct / float(oov_all)
        self.my_print("-----------")
        self.my_print("precision:%s" % (m_p))
        self.my_print("recall:%s" % (m_r))
        self.my_print("f-measure:%s" % (m_f))
        self.my_print("oov-rate:%s" % (m_oov_r))
        self.my_print("oov-recall:%s" % (m_oov))
        self.my_print("iv-recall:%s" % (m_iv))
        self.my_print("-----------")


def build_segmentation(config):
    config["fname_in"] = "data/icwb2-data/testing/pku_test.utf8"
    config["fname_out"] = "result/pku_test_out.utf8"
    config["fname_dict"] = "word_idx_dict.json"
    config["fname_eval_gold"] = "data/icwb2-data/gold/pku_test_gold.utf8"
    config["fname_eval_dict"] = "data/icwb2-data/gold/pku_training_words.utf8"
    return Segmentation(config)


def gen_test_network(config, model_name, network_class):
    config2 = copy.deepcopy(config)
    config2["batch_size"] = 1
    ntk = network_class(config2)
    ntk.load_model(model_name)
    return ntk


def run_segmentation(config, model_name, network_class):
    build_segmentation(config).run_network(
        gen_test_network(config, model_name, network_class))


def main():
    config = {
        "s_vocab": 4000,
        "s_embed": 50,
        "s_window": 5,
        "s_hidden": 300,
        "batch_size": 1
    }
    model_name = "model/model_feedforward_66.json"
    seg = build_segmentation(config)
    build_segmentation(config).run_network(
        gen_test_network(config, model_name, maxmgnet_feedforward.MaxMgNetworkFeedForward))


def eval_lstm():
    s_vocab = 4000
    s_window = 5
    batch_size = 20
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 100,
        "s_hidden": 150,
        "batch_size": batch_size,
        "p_lr": 0.2,
        "p_eps": 0.2,
        "p_mloss": 0.2,
        "p_lambda": 0.0001
    }

    model_name = "model/model_lstm_2_28.json"
    run_segmentation(config, model_name, maxmgnet_lstm.MaxMgNetworkLSTM)
    # test_maxmgnet2(model_name)


if __name__ == '__main__':
    eval_lstm()


## model2
# -----------
# precision:0.42263202708
# recall:0.584955735255
# f-measure:0.490718600174
# oov-rate:0.0575537500479
# oov-recall:0.226735475279
# iv-recall:0.606831698267
# -----------


## model7
# -----------
# precision:0.700087941857
# recall:0.663578354348
# f-measure:0.681344410505
# oov-rate:0.0575537500479
# oov-recall:0.425337106709
# iv-recall:0.678127382707
# -----------

## model10
# -----------
# precision:0.687244097696
# recall:0.646485647492
# f-measure:0.666242088529
# oov-rate:0.0575537500479
# oov-recall:0.406525719993
# iv-recall:0.661139633
# -----------

## model13
# -----------
# precision:0.695271877141
# recall:0.661058521443
# f-measure:0.677733684335
# oov-rate:0.0575537500479
# oov-recall:0.411020476111
# iv-recall:0.676327962182
# -----------

## model 20
# -----------
# precision:0.723374689826
# recall:0.698271567087
# f-measure:0.710601495695
# oov-rate:0.0575537500479
# oov-recall:0.437989012818
# iv-recall:0.714166624307
# -----------

## model 21
# -----------
# precision:0.632684874082
# recall:0.575527919365
# f-measure:0.602754433936
# oov-rate:0.0575537500479
# oov-recall:0.369568836358
# iv-recall:0.588105525339
# -----------

## model 23
#-----------
#precision:0.773619499569
#recall:0.755978614954
#f-measure:0.76469733093
#oov-rate:0.0575537500479
#oov-recall:0.522723489263
#iv-recall:0.770223148478
#-----------


# f-measure:0.911487127404 28
# f-measure:0.91325987514 32
# f-measure:0.912903303303 33
# f-measure:0.913888582036 34
# f-measure:0.914324695664 36
# f-measure:0.914215450825 37
# f-measure:0.915378516685 41


# f-measure:0.832521102848 2_5
# f-measure:0.862780699858 2_13
# f-measure:0.866091285827 2_21
# f-measure:0.868691742092 2_23
# f-measure:0.877145677348 2_28


