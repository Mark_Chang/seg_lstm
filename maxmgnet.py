import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import logging
from profiler import Profiler, AvgCounter
from network import Network
from theano import printing
import initializer
import optimizer

np.random.seed(7)


def my_print_fn(op, xin):
    for attr in op.attrs:
        temp = getattr(xin, attr)
        if callable(temp):
            pmsg = temp()
        else:
            pmsg = temp
        print(op.message, attr, '=', pmsg)


def my_print_var(m, x, off=False):
    if off:
        return x
    else:
        return printing.Print(message=m, global_fn=my_print_fn)(x)


class MaxMgNetwork(Network):
    def __init__(self, config, datagen=None):
        super(MaxMgNetwork, self).__init__(config, datagen)
        self.s_vocab = config["s_vocab"]
        self.s_embed = config["s_embed"]
        self.s_window = config["s_window"]
        self.s_hidden = config["s_hidden"]
        self.batch_size = config["batch_size"]
        self.s_output = 4

        self.p_lambda = config.get("p_lambda", 0.0001)
        self.p_mloss = config.get("p_mloss", 0.2)
        self.p_rho = self.config.get("p_rho", 0.9)
        self.p_lr = self.config.get("p_lr", 2.0)
        self.p_eps = self.config.get("p_eps", 0)
        self.p_optimizer = self.config.get("p_optimizer", "clipped_adagrad")
        self.is_train = self.config.get("is_train", True)

    def build_input(self):
        self.input["x_in"] = T.itensor3('x_in')
        self.input["yp_in"] = T.imatrix('yp_in')
        self.input["yg_in"] = T.imatrix('yg_in')
        self.input["mask_in"] = T.imatrix('mask_in')

    def viterbi_batch_numpy(self, y_results_batch, mask_in, y_gold_batch=None):
        results = np.zeros(mask_in.shape).astype(int)
        for i in range(y_results_batch.shape[1]):
            length = sum(mask_in[:, i])
            result_raw = self.viterbi_numpy(y_results_batch[:length, i, :], y_gold_batch[:length, i])
            for j, rval in enumerate(result_raw):
                results[j, i] = rval
        return results

    def viterbi_numpy(self, y_results, y_gold=None):
        WY = self.wb["w_y"].get_value()
        y_tags = np.zeros(y_results.shape).astype(int)
        y_vals = np.ones(y_results.shape)
        for i, y_result in enumerate(y_results):
            for j in range(4):
                if i > 0:
                    temp_result = y_result[j] + WY[:, j] + y_vals[i - 1, :]
                    if y_gold is not None:
                        temp_result += self.p_mloss * int(j != y_gold[i])
                    temp_val_max = np.max(temp_result)
                    temp_tag_max = np.argmax(temp_result)
                    y_vals[i, j] = temp_val_max
                    y_tags[i, j] = temp_tag_max
                else:
                    y_vals[i, j] = y_result[j]
                    if y_gold is not None:
                        y_vals[i, j] += self.p_mloss * int(j != y_gold[i])
        y_tag_seq = []
        next_tag = int(np.argmax(y_vals[-1, :]))
        y_tag_seq.append(next_tag)
        for i in range(1, y_results.shape[0]):
            next_tag = int(y_tags[-i, next_tag])
            y_tag_seq.append(next_tag)
        y_tag_seq.reverse()
        return y_tag_seq

    def build_wb(self, wb_shape):
        for wb_s_key in wb_shape.keys():
            self.wb[wb_s_key] = initializer.weight_init(wb_shape[wb_s_key])
        temp_shape = self.wb["w_y"].get_value().shape
        self.wb["w_y"].set_value((np.zeros(temp_shape)).astype(theano.config.floatX))

    def build_networks(self, wb_shape, scan_builder):
        self.build_input()
        self.build_wb(wb_shape)
        yseq_scan, trainer_scan = scan_builder()
        self.build_yseq_func(yseq_scan)
        if self.is_train:
            self.build_trainer(trainer_scan)

    def build_yseq_func(self, yseq_scan):
        self.func_y_seq = theano.function(
            inputs=[self.input["x_in"]],
            outputs=yseq_scan,
            allow_input_downcast=True
        )

    def build_trainer(self, trainer_scan):

        s_seq_pred = trainer_scan[-2]
        s_seq_gold = trainer_scan[-1]

        mask_avg = T.sum(T.cast(self.input["mask_in"], theano.config.floatX), axis=0)

        mismatch = T.sum(T.cast(T.neq(self.input["yp_in"], self.input["yg_in"]), theano.config.floatX),
                         axis=0)

        delta_sum = self.p_mloss * mismatch

        mismatch_print = my_print_var("mismatch", mismatch, True)

        mask_avg_print = my_print_var("mask_avg", mask_avg, True)

        error = T.sum(mismatch_print / mask_avg_print) / self.batch_size

        s_seq_pred_sum = T.sum(s_seq_pred, axis=0)

        s_seq_gold_sum = T.sum(s_seq_gold, axis=0)

        reg_cost = optimizer.l2_regularizer(self.wb)

        pre_error_cost = T.maximum(0, s_seq_pred_sum + delta_sum - s_seq_gold_sum)

        pre_error_cost_print = my_print_var("pre_error", pre_error_cost, True)

        error_cost = T.sum(pre_error_cost_print / mask_avg_print) / self.batch_size

        cost = error_cost + self.p_lambda * reg_cost

        gd = OrderedDict()
        for wb_key in self.wb.keys():
            gd[wb_key] = T.grad(cost=cost, wrt=self.wb[wb_key])

        if self.p_optimizer == "adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)
        elif self.p_optimizer == "clipped_adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.clipped_adagrad_update(self.wb, gd.values(), self.p_lr,
                                                                                   self.p_eps)
        elif self.p_optimizer == "adadelta":
            wb_updates, gradients_sq, deltas_sq = optimizer.adadelta_updates(self.wb, gd.values(), self.p_rho,
                                                                             self.p_eps)
        else:
            wb_updates, gradients_sq, deltas_sq = optimizer.sgd_update(self.wb, gd.values(), self.p_lr)

        self.gradients_sq.update(gradients_sq)
        self.deltas_sq.update(deltas_sq)

        self.func_trainer = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            updates=wb_updates,
            allow_input_downcast=True
        )

        self.func_validator = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            allow_input_downcast=True
        )

    def y_seq_func(self, x_in):
        results = self.func_y_seq(x_in)
        return results[-1]

    def t_func(self, x_in, y_gold, mask_in):
        if not self.is_train:
            return None
        result1 = self.func_y_seq(x_in)
        y_pred = self.viterbi_batch_numpy(result1[-1], mask_in, y_gold)
        cost = self.func_trainer(x_in, y_pred, y_gold, mask_in)
        return cost

    def v_func(self, x_in, y_gold, mask_in):
        if not self.is_train:
            return None
        result1 = self.func_y_seq(x_in)
        y_pred = self.viterbi_batch_numpy(result1[-1], mask_in, y_gold)
        cost = self.func_validator(x_in, y_pred, y_gold, mask_in)
        return cost


if __name__ == "__main__":
    print 1
