import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import my_logger
import logging
from profiler import Profiler, AvgCounter


class DataGenerator(object):
    def __init__(self, params):
        self.pad_id = params["pad_id"]
        self.s_window = params["s_window"]
        self.train_file_idx = params["train_file_idx"]
        self.valid_file_idx = params["valid_file_idx"]
        self.random_preload = params.get("random_preload",True)
        self.my_print = my_logger.my_print

    def read_input_file(self, is_train=True):
        if is_train:
            file_to_read = self.train_file_idx
        else:
            file_to_read = self.valid_file_idx
        lines = [line for line in read_file_line(file_to_read, False)]
        if self.random_preload:
            np.random.shuffle(lines)
        for line in lines:
            if len(line.strip()) > 0:
                yield line

    def gen_input_line(self, line):
        pad_id = self.pad_id
        s_window = self.s_window
        line_word = [pad_id] * (s_window / 2) + [int(w.split(",")[0]) for w in line.strip().split(" ")] + [pad_id] * (
            s_window / 2)
        case_raw = [line_word[i:i + s_window] for i in range(len(line_word) + 1 - s_window)]
        line_tag = [int(w.split(",")[1]) for w in line.strip().split(" ")]

        # case_all = zip(case_raw, line_tag)
        ## np.random.shuffle(case_all)
        # [case_x, case_y] = zip(*case_all)
        return case_raw, line_tag

    def gen_input_data(self, is_train=True):
        for line in self.read_input_file(is_train):
            case_x, case_y = self.gen_input_line(line)
            yield case_x, case_y


class BatchDataGenerator(DataGenerator):
    def __init__(self, params):
        super(BatchDataGenerator, self).__init__(params)
        self.batch_size = params["batch_size"]
        self.train_lines = self.read_input_file(is_train=True)
        self.valid_lines = self.read_input_file(is_train=False)

    def read_input_file(self, is_train=True):
        if is_train:
            file_to_read = self.train_file_idx
        else:
            file_to_read = self.valid_file_idx
        lines = [line for line in read_file_line(file_to_read, False)]
        # lines = sorted(lines, key=lambda x: len(x), reverse=True)
        return lines

    def gen_input_data(self, is_train=True):
        if is_train:
            lines = self.train_lines
        else:
            lines = self.valid_lines
        lines_len = len(lines)
        range_idx = range(lines_len)
        batch_idxs = [range_idx[(i * self.batch_size): ((i + 1) * self.batch_size)] for i in
                      range(int(math.ceil(lines_len / float(self.batch_size))))]
        while len(batch_idxs[-1]) < self.batch_size:
            batch_idxs[-1].append(range_idx[int(math.floor(np.random.rand() * lines_len))])
        if self.random_preload:
            np.random.shuffle(batch_idxs)
        for idxs in batch_idxs:
            temp_array_x = []
            temp_array_y = []
            max_len = 0
            for idx in idxs:
                case_x, case_y = self.gen_input_line(lines[idx])
                temp_array_x.append(case_x)
                temp_array_y.append(case_y)
                max_len = max(len(case_x), max_len)
            output_x = self.pad_id * np.ones((max_len, self.batch_size, 5)).astype(int)
            output_y = np.zeros((max_len, self.batch_size)).astype(int)
            output_mask = np.zeros((max_len, self.batch_size)).astype(int)
            for i, temp_item in enumerate(zip(temp_array_x, temp_array_y)):
                temp_xs, temp_ys = temp_item
                for j in range(max_len):
                    if j < len(temp_xs):
                        t_xs = temp_xs[j]
                        t_y = temp_ys[j]
                        for k, t_x in enumerate(t_xs):
                            output_x[j, i, k] = t_x
                        output_y[j, i] = t_y
                        output_mask[j, i] = 1
                    else:
                        output_mask[j, i] = 0
            yield output_x, output_y, output_mask


class BatchTruncatedDataGenerator(DataGenerator):
    def __init__(self, params):
        super(BatchTruncatedDataGenerator, self).__init__(params)
        self.batch_size = params["batch_size"]
        self.truncated_len = params.get("truncated_len", 30)
        self.train_lines = self.read_input_file(is_train=True)
        self.valid_lines = self.read_input_file(is_train=False)

    def read_input_file(self, is_train=True):
        if is_train:
            file_to_read = self.train_file_idx
        else:
            file_to_read = self.valid_file_idx
        lines = [line for line in read_file_line(file_to_read, False)]
        return lines

    def gen_input_data(self, is_train=True):
        if is_train:
            lines = self.train_lines
        else:
            lines = self.valid_lines
        batch_idxs = range(len(lines))
        #if self.random_preload:
        #    np.random.shuffle(batch_idxs)
        temp_array_x = []
        temp_array_y = []
        for idx in batch_idxs:
            case_x, case_y = self.gen_input_line(lines[idx])
            temp_array_x.extend(case_x)
            temp_array_y.extend(case_y)
            if len(temp_array_x) > self.truncated_len * self.batch_size:
                result_x = []
                result_y = []
                for i in range(self.batch_size):
                    result_x.append(temp_array_x[i * self.truncated_len:(i + 1) * self.truncated_len])
                    result_y.append(temp_array_y[i * self.truncated_len:(i + 1) * self.truncated_len])
                output_x = np.swapaxes(np.array(result_x), 0, 1)
                output_y = np.swapaxes(np.array(result_y), 0, 1)
                output_mask = np.ones(output_y.shape)
                temp_array_x = temp_array_x[self.truncated_len * self.batch_size:]
                temp_array_y = temp_array_y[self.truncated_len * self.batch_size:]
                yield output_x, output_y, output_mask

        msg = "remaining: %s " % (len(temp_array_x))
        self.my_print(msg)


class SeqDataGenerator(DataGenerator):
    def __init__(self, params):
        super(SeqDataGenerator, self).__init__(params)
        self.input_size = params.get("input_size", 10000)
        self.remaining_size = params.get("remaining_size", 100)

    def gen_input_data(self, is_train=True):
        case_x_out = []
        case_y_out = []
        for line in self.read_input_file(is_train):
            case_x, case_y = self.gen_input_line(line)
            case_x_out.extend(case_x)
            case_y_out.extend(case_y)
            if len(case_x_out) >= self.input_size:
                yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)
                case_x_out = []
                case_y_out = []
        msg = "remaining: %s " % (len(case_x_out))
        self.my_print(msg)
        if len(case_x_out) >= self.remaining_size:
            yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)


class SegNegSampDataGenerator(SeqDataGenerator):
    def __init__(self, params):
        super(SegNegSampDataGenerator, self).__init__(params)
        self.s_negsamp = params.get("s_negsamp", params["s_vocab"])

    def set_nagsamp_size(self, s):
        self.s_negsamp = s

    def gen_neg_idx(self, target):
        while True:
            # result = #negsamp_array[int(math.floor(np.random.rand() * len(negsamp_array)))]
            result = int(math.floor(np.random.rand() * (self.s_negsamp)))
            if result != target:
                return result

    def gen_input_line(self, line):
        line_word = [self.pad_id] * (self.s_window / 2) + [int(w.split(",")[0]) for w in line.strip().split(" ")] \
                    + [self.pad_id] * (self.s_window / 2)
        case_raw = [line_word[i:i + self.s_window] for i in range(len(line_word) + 1 - self.s_window)]
        case_positive_raw = zip(case_raw, [1] * len(case_raw))
        case_negative_raw = zip(
            [copy.deepcopy(item[:self.s_window / 2]) + [self.gen_neg_idx(item[self.s_window / 2])] + copy.deepcopy(
                item[self.s_window / 2 + 1:])
             for item in case_raw], [0] * len(case_raw))
        case_all = case_positive_raw + case_negative_raw
        np.random.shuffle(case_all)
        [case_x, case_y] = zip(*case_all)
        case_x = list(case_x)
        case_y = list(case_y)
        return case_x, case_y


def main():
    params = {
        "pad_id": 3999,
        "s_window": 5,
        "train_file_idx": "./data/converted/pku_valid_c.utf8",
        "valid_file_idx": "./data/converted/pku_valid_c.utf8",
        "batch_size": 8
    }
    # data_gen = DataGenerator(params)
    # for x, y in data_gen.gen_input_data():
    #    write_json("example_xy.json", {"x": x, "y": y}, indent=4)
    #    break
    #    print 1
    data_gen = BatchTruncatedDataGenerator(params)
    # data_gen.gen_input_data()

    for x, y, m in data_gen.gen_input_data():
        write_json("example_batch_xy.json", {"x": x.tolist(), "y": y.tolist(), "m": m.tolist()}, indent=4)


if __name__ == "__main__":
    main()
