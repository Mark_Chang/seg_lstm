import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import logging
from profiler import Profiler, AvgCounter
from network import Network
import initializer
import optimizer

np.random.seed(7)


class LSTMNetwork(Network):
    def __init__(self, config, datagen=None):
        super(LSTMNetwork, self).__init__(config, datagen)
        self.s_vocab = config["s_vocab"]
        self.s_embed = config["s_embed"]
        self.s_window = config["s_window"]
        self.s_hidden = config["s_hidden"]
        self.batch_size = config["batch_size"]
        self.s_output = 4

        self.p_lambda = config.get("p_lambda", 0.0001)
        self.p_mloss = config.get("p_mloss", 0.2)
        self.p_rho = self.config.get("p_rho", 0.9)
        self.p_lr = self.config.get("p_lr", 2.0)
        self.p_eps = self.config.get("p_eps", 0)
        self.p_optimizer = self.config.get("p_optimizer", "clipped_adagrad")
        self.is_train = self.config.get("is_train", True)

        self.build_network_wrapper()

    def build_input(self):
        self.input["x_in"] = T.itensor3('x_in')
        self.input["yp_in"] = T.imatrix('yp_in')
        self.input["yg_in"] = T.imatrix('yg_in')
        self.input["mask_in"] = T.imatrix('mask_in')

    def viterbi_batch_numpy(self, y_results_batch, mask_in):
        results = np.zeros(mask_in.shape).astype(int)
        for i in range(y_results_batch.shape[1]):
            length = sum(mask_in[:, i])
            result_raw = self.viterbi_numpy(y_results_batch[:length, i, :])
            for j, rval in enumerate(result_raw):
                results[j, i] = rval
        return results

    def viterbi_numpy(self, y_results):
        WY = self.wb["w_y"].get_value()
        y_tags = np.zeros(y_results.shape).astype(int)
        y_vals = np.ones(y_results.shape)
        for i, y_result in enumerate(y_results):
            for j in range(4):
                if i > 0:
                    temp_result = y_result[j] + WY[:, j] + y_vals[i - 1, :]
                    temp_val_max = np.max(temp_result)
                    temp_tag_max = np.argmax(temp_result)
                    y_vals[i, j] = temp_val_max
                    y_tags[i, j] = temp_tag_max
                else:
                    y_vals[i, j] = y_result[j]
        y_tag_seq = []
        next_tag = int(np.argmax(y_vals[-1, :]))
        y_tag_seq.append(next_tag)
        for i in range(1, y_results.shape[0]):
            next_tag = int(y_tags[-i, next_tag])
            y_tag_seq.append(next_tag)
        y_tag_seq.reverse()
        return y_tag_seq


    def build_wb(self, wb_shape):
        for wb_s_key in wb_shape.keys():
            self.wb[wb_s_key] = initializer.weight_init(wb_shape[wb_s_key])
        temp_shape = self.wb["w_y"].get_value().shape
        self.wb["w_y"].set_value((np.zeros(temp_shape)).astype(theano.config.floatX))

    def build_networks(self, wb_shape, scan_builder):
        self.build_input()
        self.build_wb(wb_shape)
        yseq_scan, trainer_scan = scan_builder()
        self.build_yseq_func(yseq_scan)
        if self.is_train:
            self.build_trainer(trainer_scan)

    def build_yseq_func(self, yseq_scan):
        self.func_y_seq = theano.function(
            inputs=[self.input["x_in"]],
            outputs=yseq_scan,
            allow_input_downcast=True
        )

    def build_trainer(self, trainer_scan):

        s_seq_pred = trainer_scan[-2]
        s_seq_gold = trainer_scan[-1]

        mask_avg = T.sum(T.cast(self.input["mask_in"], theano.config.floatX), axis=0)

        mismatch = T.sum(T.cast(T.neq(self.input["yp_in"], self.input["yg_in"]), theano.config.floatX),
                         axis=0)

        delta_sum = self.p_mloss * mismatch

        error =  (T.sum(mismatch) / (self.input["yp_in"].shape[0]*self.input["yp_in"].shape[1]))

        s_seq_pred_sum = T.sum(s_seq_pred, axis=0)

        s_seq_gold_sum = T.sum(s_seq_gold, axis=0)

        reg_cost = optimizer.l2_regularizer(self.wb)

        error_cost = T.sum(T.maximum(0, s_seq_pred_sum + delta_sum - s_seq_gold_sum) / mask_avg) / self.batch_size

        cost = error_cost + self.p_lambda * reg_cost

        gd = OrderedDict()
        for wb_key in self.wb.keys():
            gd[wb_key] = T.grad(cost=cost, wrt=self.wb[wb_key])

        if self.p_optimizer == "adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)
        elif self.p_optimizer == "clipped_adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.clipped_adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)
        elif self.p_optimizer =="adadelta":
            wb_updates, gradients_sq, deltas_sq = optimizer.adadelta_updates(self.wb, gd.values(), self.p_rho, self.p_eps)
        else:
            wb_updates, gradients_sq, deltas_sq = optimizer.sgd_update(self.wb, gd.values(), self.p_lr)

        self.gradients_sq = gradients_sq
        self.deltas_sq = deltas_sq

        self.func_trainer = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            updates=wb_updates,
            allow_input_downcast=True
        )

        self.func_validator = theano.function(
            inputs=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            allow_input_downcast=True
        )


    def y_seq_func(self, x_in):
        results = self.func_y_seq(x_in)
        return results[-1]

    def t_func(self, x_in, y_gold, mask_in):
        if not self.is_train:
            return None
        result1 = self.func_y_seq(x_in)
        y_pred = self.viterbi_batch_numpy(result1[-1], mask_in)
        cost = self.func_trainer(x_in, y_pred, y_gold, mask_in)
        return cost

    def v_func(self, x_in, y_gold, mask_in):
        if not self.is_train:
            return None
        result1 = self.func_y_seq(x_in)
        y_pred = self.viterbi_batch_numpy(result1[-1], mask_in)
        cost = self.func_validator(x_in, y_pred, y_gold, mask_in)
        return cost

    def core_lstm(self, x_in, h_tm1, c_tm1):
        x_in_1dim = T.reshape(x_in, newshape=(self.batch_size * self.s_window,))
        emb_lookup = self.wb["w_embed"][x_in_1dim]
        x_t = T.reshape(emb_lookup, newshape=(self.batch_size, self.s_embed * self.s_window))

        x_cin = T.tanh(T.dot(x_t, self.wb["w_cx"]) + T.dot(h_tm1, self.wb["w_ch"]) + self.wb["b_c"])

        i_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ix"]) +
                             T.dot(c_tm1, self.wb["w_ic"]) + T.dot(h_tm1, self.wb["w_ih"]) + self.wb["b_i"])

        f_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_fx"]) +
                             T.dot(c_tm1, self.wb["w_fc"]) + T.dot(h_tm1, self.wb["w_fh"]) + self.wb["b_f"])

        c_t = T.mul(f_t, c_tm1) + T.mul(i_t, x_cin)

        o_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ox"]) +
                             T.dot(c_t, self.wb["w_oc"]) + T.dot(h_tm1, self.wb["w_oh"]) + self.wb["b_o"])
        h_t = T.mul(o_t, T.tanh(c_t))

        y_result = T.nnet.softmax(T.dot(h_t, self.wb["w_out"]) + self.wb["b_out"])

        return h_t, c_t, y_result

    #def step_lstm_yseq(self, x_in, h_tm1, c_tm1):
    #    h_t, c_t, y_result = self.core_lstm(x_in, h_tm1, c_tm1)
    #    return h_t, c_t, y_result

    #def step_lstm_selected_two(self, x_in, y_pred, y_gold, mask_in, h_tm1, c_tm1, y_pred_tm1, y_gold_tm1):
    #    h_t, c_t, y_results = self.core_lstm(x_in, h_tm1, c_tm1)
    #    s_pred = mask_in * (y_results[T.arange(self.batch_size), y_pred] + self.wb["w_y"][y_pred_tm1, y_pred])
    #    s_gold = mask_in * (y_results[T.arange(self.batch_size), y_gold] + self.wb["w_y"][y_gold_tm1, y_gold])
    #    return h_t, c_t, y_pred, y_gold, s_pred, s_gold

    def scan_builder(self):
        h_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))
        c_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))

        yseq_scan, _ = theano.scan(
            self.core_lstm,
            sequences=[self.input["x_in"]],
            outputs_info=[h_0, c_0, None]
        )
        #trainer_scan, _ = theano.scan(
        #    self.step_lstm_selected_two,
        #    sequences=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
        #    outputs_info=[h_0, c_0, yp_0, yg_0, None, None]
        #)
        return yseq_scan

    def build_network_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),

            "w_cx": (self.s_embed * self.s_window, self.s_hidden),
            "w_ch": (self.s_hidden, self.s_hidden),
            "b_c": (self.s_hidden,),

            "w_ix": (self.s_embed * self.s_window, self.s_hidden),
            "w_ih": (self.s_hidden, self.s_hidden),
            "w_ic": (self.s_hidden, self.s_hidden),
            "b_i": (self.s_hidden,),

            "w_fx": (self.s_embed * self.s_window, self.s_hidden),
            "w_fh": (self.s_hidden, self.s_hidden),
            "w_fc": (self.s_hidden, self.s_hidden),
            "b_f": (self.s_hidden,),

            "w_ox": (self.s_embed * self.s_window, self.s_hidden),
            "w_oh": (self.s_hidden, self.s_hidden),
            "w_oc": (self.s_hidden, self.s_hidden),
            "b_o": (self.s_hidden,),

            "w_out": (self.s_hidden, self.s_output),
            "b_out": (self.s_output,),
            "w_y": (self.s_output, self.s_output)
        })

        self.build_networks(wb_shape=wb_shape, scan_builder=self.scan_builder)


if __name__ == "__main__":
    print 1
