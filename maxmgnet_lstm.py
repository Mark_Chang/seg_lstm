import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import logging
from maxmgnet import MaxMgNetwork
import initializer
import optimizer

np.random.seed(7)


class MaxMgNetworkLSTM(MaxMgNetwork):
    def __init__(self, config, datagen=None):
        super(MaxMgNetworkLSTM, self).__init__(config, datagen)
        self.build_network_wrapper()

    def core_lstm(self, x_in, h_tm1, c_tm1):
        x_in_1dim = T.reshape(x_in, newshape=(self.batch_size * self.s_window,))
        emb_lookup = self.wb["w_embed"][x_in_1dim]
        x_t = T.reshape(emb_lookup, newshape=(self.batch_size, self.s_embed * self.s_window))

        x_cin = T.tanh(T.dot(x_t, self.wb["w_cx"]) + T.dot(h_tm1, self.wb["w_ch"]) + self.wb["b_c"])

        i_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ix"]) +
                             T.dot(c_tm1, self.wb["w_ic"]) + T.dot(h_tm1, self.wb["w_ih"]) + self.wb["b_i"])

        f_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_fx"]) +
                             T.dot(c_tm1, self.wb["w_fc"]) + T.dot(h_tm1, self.wb["w_fh"]) + self.wb["b_f"])

        c_t = T.mul(f_t, c_tm1) + T.mul(i_t, x_cin)

        o_t = T.nnet.sigmoid(T.dot(x_t, self.wb["w_ox"]) +
                             T.dot(c_t, self.wb["w_oc"]) + T.dot(h_tm1, self.wb["w_oh"]) + self.wb["b_o"])
        h_t = T.mul(o_t, T.tanh(c_t))

        y_result = T.dot(h_t, self.wb["w_out"]) + self.wb["b_out"]

        return h_t, c_t, y_result

    def step_lstm_yseq(self, x_in, h_tm1, c_tm1):
        h_t, c_t, y_result = self.core_lstm(x_in, h_tm1, c_tm1)

        return h_t, c_t, y_result

    def step_lstm_selected_two(self, x_in, y_pred, y_gold, mask_in, h_tm1, c_tm1, y_pred_tm1, y_gold_tm1):
        h_t, c_t, y_results = self.core_lstm(x_in, h_tm1, c_tm1)
        s_pred = mask_in * (y_results[T.arange(self.batch_size), y_pred] + self.wb["w_y"][y_pred_tm1, y_pred])
        s_gold = mask_in * (y_results[T.arange(self.batch_size), y_gold] + self.wb["w_y"][y_gold_tm1, y_gold])
        return h_t, c_t, y_pred, y_gold, s_pred, s_gold

    def scan_builder(self):
        h_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))
        c_0 = theano.shared(np.zeros((self.batch_size, self.s_hidden)))

        yp_0 = T.cast(theano.shared(np.zeros((self.batch_size,))), "int32")
        yg_0 = T.cast(theano.shared(np.zeros((self.batch_size,))), "int32")

        yseq_scan, _ = theano.scan(
            self.step_lstm_yseq,
            sequences=[self.input["x_in"]],
            outputs_info=[h_0, c_0, None]
        )
        trainer_scan, _ = theano.scan(
            self.step_lstm_selected_two,
            sequences=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs_info=[h_0, c_0, yp_0, yg_0, None, None]
        )
        return yseq_scan, trainer_scan

    def build_network_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),

            "w_cx": (self.s_embed * self.s_window, self.s_hidden),
            "w_ch": (self.s_hidden, self.s_hidden),
            "b_c": (self.s_hidden,),

            "w_ix": (self.s_embed * self.s_window, self.s_hidden),
            "w_ih": (self.s_hidden, self.s_hidden),
            "w_ic": (self.s_hidden, self.s_hidden),
            "b_i": (self.s_hidden,),

            "w_fx": (self.s_embed * self.s_window, self.s_hidden),
            "w_fh": (self.s_hidden, self.s_hidden),
            "w_fc": (self.s_hidden, self.s_hidden),
            "b_f": (self.s_hidden,),

            "w_ox": (self.s_embed * self.s_window, self.s_hidden),
            "w_oh": (self.s_hidden, self.s_hidden),
            "w_oc": (self.s_hidden, self.s_hidden),
            "b_o": (self.s_hidden,),

            "w_out": (self.s_hidden, self.s_output),
            "b_out": (self.s_output,),
            "w_y": (self.s_output, self.s_output)
        })

        self.build_networks(wb_shape=wb_shape, scan_builder=self.scan_builder)


def main():
    json_item = read_json("example_batch_xy.json")
    config = {
        "s_vocab": 4000,
        "s_embed": 100,
        "s_window": 5,
        "s_hidden": 150,
        "batch_size": 8,
        "p_lr": 0.2,
        "p_eps": 0.2,
        "p_mloss": 0.2,
        "p_lambda": 0.0001
    }
    mmnk = MaxMgNetworkLSTM(config)
    # result_test = np.random.rand(5, 1, 4)
    # seq1 = mmnk.viterbi_numpy(result_test)
    x_in = np.array(json_item["x"])
    y_gold = np.array(json_item["y"])
    mask_in = np.array(json_item["m"])
    result1 = mmnk.func_y_seq(x_in)
    print result1
    y_pred = mmnk.viterbi_batch_numpy(result1[-1], mask_in)
    print y_pred
    cost = mmnk.func_trainer(x_in, y_pred, y_gold, mask_in)
    print cost


if __name__ == "__main__":
    main()
