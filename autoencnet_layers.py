import theano.tensor as T
from collections import OrderedDict
from autoencnet import AutoEncNetwork


class AutoEncNetwork1Layer(AutoEncNetwork):
    def __init__(self, config, datagen=None):
        super(AutoEncNetwork1Layer, self).__init__(config, datagen)
        self.s_hidden = config["s_hidden"]
        self.s_out = 1
        self.build_networks()

    def build_wb_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_hidden": (self.s_embed * self.s_window, self.s_hidden),
            "b_hidden": (self.s_hidden,),
            "w_out": (self.s_hidden, self.s_out),
            "b_out": (self.s_out,),

        })
        return wb_shape

    def build_hidden_layer(self, hidden_input):
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        return hidden_result


class AutoEncNetwork2Layer(AutoEncNetwork):
    def __init__(self, config, datagen=None):
        super(AutoEncNetwork2Layer, self).__init__(config, datagen)
        self.s_hidden = config["s_hidden"]
        self.s_hidden2 = config["s_hidden2"]
        self.s_out = 1
        self.build_networks()


    def build_wb_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_hidden": (self.s_embed * self.s_window, self.s_hidden),
            "b_hidden": (self.s_hidden,),
            "w_hidden2": (self.s_hidden, self.s_hidden2),
            "b_hidden2": (self.s_hidden2,),
            "w_out": (self.s_hidden2, self.s_out),
            "b_out": (self.s_out,),

        })
        return wb_shape

    def build_hidden_layer(self, hidden_input):
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        hidden2_result = T.tanh(T.dot(hidden_result, self.wb["w_hidden2"]) + self.wb["b_hidden2"])
        return hidden2_result


class AutoEncNetwork3Layer(AutoEncNetwork):
    def __init__(self, config, datagen=None):
        super(AutoEncNetwork3Layer, self).__init__(config, datagen)
        self.s_hidden = config["s_hidden"]
        self.s_hidden2 = config["s_hidden2"]
        self.s_hidden3 = config["s_hidden3"]
        self.s_out = 1
        self.build_networks()


    def build_wb_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_hidden": (self.s_embed * self.s_window, self.s_hidden),
            "b_hidden": (self.s_hidden,),
            "w_hidden2": (self.s_hidden, self.s_hidden2),
            "b_hidden2": (self.s_hidden2,),
            "w_hidden3": (self.s_hidden2, self.s_hidden3),
            "b_hidden3": (self.s_hidden3,),
            "w_out": (self.s_hidden3, self.s_out),
            "b_out": (self.s_out,),

        })
        return wb_shape

    def build_hidden_layer(self, hidden_input):
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        hidden2_result = T.tanh(T.dot(hidden_result, self.wb["w_hidden2"]) + self.wb["b_hidden2"])
        hidden3_result = T.tanh(T.dot(hidden2_result, self.wb["w_hidden3"]) + self.wb["b_hidden3"])
        return hidden3_result

if __name__ == "__main__":
    print 1
