import logging

def set_logfile(logfile):
    logging.basicConfig(filename=logfile, format='%(asctime)s [%(levelname)s]:%(message)s', level=logging.INFO,
                        datefmt='%Y-%m-%d %I:%M:%S')

def my_print(s):
    print s
    logging.info(s)

#set_logfile(logfile="out.log")
