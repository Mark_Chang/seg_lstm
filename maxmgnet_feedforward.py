import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json, write_json
import logging
from profiler import Profiler, AvgCounter
from network import Network
import initializer
import optimizer
from maxmgnet import MaxMgNetwork

np.random.seed(7)


class MaxMgNetworkFeedForward(MaxMgNetwork):
    def __init__(self, config, datagen=None):
        super(MaxMgNetworkFeedForward, self).__init__(config, datagen)
        self.build_network_wrapper()

    def core_feedforward(self, x_in):
        x_in_1dim = T.reshape(x_in, newshape=(self.batch_size * self.s_window,))
        emb_lookup = self.wb["w_embed"][x_in_1dim]
        hidden_input = T.reshape(emb_lookup, newshape=(self.batch_size, self.s_embed * self.s_window))
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        y_results = T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"]
        return y_results

    def step_feedforward_yseq(self, x_in):
        y_results = self.core_feedforward(x_in)
        return x_in, y_results

    def step_feedforward_selected_two(self, x_in, y_pred, y_gold, mask_in, y_pred_tm1, y_gold_tm1):
        y_results = self.core_feedforward(x_in)
        s_pred = mask_in * (y_results[T.arange(self.batch_size), y_pred] + self.wb["w_y"][y_pred_tm1, y_pred])
        s_gold = mask_in * (y_results[T.arange(self.batch_size), y_gold] + self.wb["w_y"][y_gold_tm1, y_gold])
        return y_pred, y_gold, s_pred, s_gold


    def scan_builder(self):
        yp_0 = T.cast(theano.shared(np.zeros((self.batch_size,))), "int32")
        yg_0 = T.cast(theano.shared(np.zeros((self.batch_size,))), "int32")
        yseq_scan, _ = theano.scan(
            self.step_feedforward_yseq,
            sequences=self.input["x_in"],
            outputs_info=[None, None]
        )

        trainer_scan, _ = theano.scan(
            self.step_feedforward_selected_two,
            sequences=[self.input["x_in"], self.input["yp_in"], self.input["yg_in"], self.input["mask_in"]],
            outputs_info=[yp_0, yg_0, None, None]
        )
        return yseq_scan, trainer_scan

    def build_network_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_hidden": (self.s_embed * self.s_window, self.s_hidden),
            "b_hidden": (self.s_hidden,),
            "w_out": (self.s_hidden, self.s_output),
            "b_out": (self.s_output,),
            "w_y": (self.s_output, self.s_output)
        })
        self.build_networks(wb_shape=wb_shape, scan_builder=self.scan_builder)



        # return x_in, y_in, wb, out_result, cost, error_cost, reg_cost,


def main():
    json_item = read_json("example_batch_xy.json")
    config = {
        "s_vocab": 4000,
        "s_embed": 100,
        "s_window": 5,
        "s_hidden": 100,
        "batch_size": 8
    }
    mmnk = MaxMgNetworkFeedForward(config)

    x_in = np.array(json_item["x"])
    y_gold = np.array(json_item["y"])
    mask_in = np.array(json_item["m"])
    result1 = mmnk.func_y_seq(x_in)
    print result1
    y_pred = mmnk.viterbi_batch_numpy(result1[-1], mask_in, y_gold)
    print y_pred
    cost = mmnk.func_trainer(x_in, y_pred, y_gold, mask_in)
    print cost
    ##print y_pred.T
    ##print y_gold.T
    ##print 1


if __name__ == "__main__":
    main()
