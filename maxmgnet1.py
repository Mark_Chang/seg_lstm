import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json
import logging
from profiler import Profiler, AvgCounter
from network import Network
import initializer
import optimizer

np.random.seed(7)


class MaxMgNetwork1(object):
    def __init__(self, config):
        self.config = config
        self.s_vocab = config["s_vocab"]
        self.s_embed = config["s_embed"]
        self.s_window = config["s_window"]
        self.s_hidden = config["s_hidden"]
        self.p_lambda = config.get("p_lambda",10)
        self.s_output = 4
        self.build_networks()

    # def step(self, x_in):
    #    emb_lookup = self.wb["w_embed"][x_in]
    #    hidden_input = T.reshape(emb_lookup, newshape=(x_in.shape[0], x_in.shape[1] * self.s_embed))
    #    hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
    #    y_results = T.nnet.sigmoid(T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"])
    #    return y_results

    def viterbi_numpy(self, y_results_0):
        y_results = np.reshape(y_results_0, (y_results_0.shape[0], y_results_0.shape[2]))
        WY = self.wb["w_y"].get_value()
        y_tags = np.zeros(y_results.shape)
        y_vals = np.ones(y_results.shape)
        for i, y_result in enumerate(y_results):
            # print y_result
            for j in range(4):
                if i > 0:
                    temp_result = y_result[j] + WY[:, j] + y_vals[i - 1, :]
                    temp_val_max = np.max(temp_result)
                    temp_tag_max = np.argmax(temp_result)
                    y_vals[i, j] = temp_val_max
                    y_tags[i, j] = temp_tag_max
                else:
                    y_vals[i, j] = y_result[j]

        y_tag_seq = []
        next_tag = int(np.argmax(y_vals[-1, :]))
        y_tag_seq.append(next_tag)
        for i in range(1, y_results.shape[0]):
            next_tag = int(y_tags[-i, next_tag])
            y_tag_seq.append(next_tag)
        y_tag_seq.reverse()
        return y_tag_seq

    def step_yseq_all(self, x_in):
        emb_lookup = self.wb["w_embed"][x_in]
        hidden_input = T.reshape(emb_lookup, newshape=(1, self.s_embed * self.s_window))
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        y_results = T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"]
        return y_results

    def step_sseq_selected(self, x_in, y_in, y_in_tm1, s_tm1):
        emb_lookup = self.wb["w_embed"][x_in]
        hidden_input = T.reshape(emb_lookup, newshape=(1, self.s_embed * self.s_window))
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        y_results = T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"]
        s_t = s_tm1 + y_results[0, y_in] + self.wb["w_y"][y_in_tm1, y_in]
        return y_in, s_t

    def step_sseq_selected_two(self, x_in, y_pred, y_gold, y_pred_tm1, y_gold_tm1, s_pred_tm1, s_gold_tm1):
        emb_lookup = self.wb["w_embed"][x_in]
        hidden_input = T.reshape(emb_lookup, newshape=(1, self.s_embed * self.s_window))
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_hidden"]) + self.wb["b_hidden"])
        y_results = T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"]
        s_pred = s_pred_tm1 + y_results[0, y_pred] + self.wb["w_y"][y_pred_tm1, y_pred]
        s_gold = s_gold_tm1 + y_results[0, y_gold] + self.wb["w_y"][y_gold_tm1, y_gold]
        return y_pred, y_gold, s_pred, s_gold

    def build_networks(self):
        self.wb = OrderedDict()
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_hidden": (self.s_embed * self.s_window, self.s_hidden),
            "b_hidden": (self.s_hidden,),
            "w_out": (self.s_hidden, self.s_output),
            "b_out": (self.s_output,),
            "w_y": (self.s_output, self.s_output)
        })
        for wb_s_key in wb_shape.keys():
            self.wb[wb_s_key] = initializer.weight_init(wb_shape[wb_s_key])

        x_in = T.imatrix('x_in')
        yp_in = T.imatrix('yp_in')
        yg_in = T.imatrix('yg_in')
        yp_0 = T.cast(theano.shared(np.zeros((1,))), "int32")
        sp_0 = theano.shared(np.zeros((1,)))
        yg_0 = T.cast(theano.shared(np.zeros((1,))), "int32")
        sg_0 = theano.shared(np.zeros((1,)))

        y_seq_all, _ = theano.scan(
            self.step_yseq_all,
            sequences=x_in,
            outputs_info=None
        )

        s_seq_selected, _ = theano.scan(
            self.step_sseq_selected,
            sequences=[x_in, yp_in],
            outputs_info=[yp_0, sp_0]
        )

        s_seq_selected_two, _ = theano.scan(
            self.step_sseq_selected_two,
            sequences=[x_in, yp_in, yg_in],
            outputs_info=[yp_0, yg_0, sp_0, sg_0]
        )

        self.func_y_seq = theano.function(
            inputs=[x_in],
            outputs=y_seq_all,
            allow_input_downcast=True
        )

        ## test1
        self.func_s_seq = theano.function(
            inputs=[x_in, yp_in],
            outputs=s_seq_selected[1][-1],
            allow_input_downcast=True
        )

        ## test2
        self.func_s_seq_two = theano.function(
            inputs=[x_in, yp_in, yg_in],
            outputs=s_seq_selected_two,
            allow_input_downcast=True
        )

        _, _, s_seq_pred, s_seq_gold = s_seq_selected_two

        delta = T.sum(T.cast(T.neq(yp_in, yg_in), theano.config.floatX))

        reg_cost = optimizer.l2_regularizer(self.wb)

        error_cost = T.maximum(0, s_seq_pred[-1, 0] + delta - s_seq_gold[-1, 0]) / yp_in.shape[0]

        cost = error_cost + self.p_lambda * reg_cost

        self.p_rho = self.config.get("p_rho", 0.9)
        self.p_eta = self.config.get("p_eta", 0.1)
        self.p_eps = self.config.get("p_eps", 1.0)

        self.p_optimizer = self.config.get("p_optimizer", "adagrad")

        gd = OrderedDict()
        for wb_key in self.wb.keys():
            gd[wb_key] = T.grad(cost=cost, wrt=self.wb[wb_key])

        if self.p_optimizer == "adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.adagrad_update(self.wb, gd.values(), self.p_eta, self.p_eps,
                                                                           None)

        self.gradients_sq = gradients_sq
        self.deltas_sq = deltas_sq

        # test2
        self.func_trainer_0 = theano.function(
            inputs=[x_in, yp_in, yg_in],
            outputs=[s_seq_pred, s_seq_gold, delta, cost],
            allow_input_downcast=True
        )

        self.func_trainer = theano.function(
            inputs=[x_in, yp_in, yg_in],
            outputs=[cost, error_cost, reg_cost],
            updates=wb_updates,
            allow_input_downcast=True
        )

        self.func_validator = theano.function(
            inputs=[x_in, yp_in, yg_in],
            outputs=[cost, error_cost, reg_cost],
            allow_input_downcast=True
        )

        self.params = {
            'wb': self.wb,
            'gradients_sq':self.gradients_sq,
            'deltas_sq':self.deltas_sq,
        }

    def t_func(self, x_in, y_gold_raw):
        result1 = self.func_y_seq(x_in)
        y_pred_raw = self.viterbi_numpy(result1)
        y_gold = np.expand_dims(np.array(y_gold_raw), axis=1)
        y_pred = np.expand_dims(np.array(y_pred_raw), axis=1)
        cost = self.func_trainer(x_in, y_pred, y_gold)
        return cost

    def v_func(self, x_in, y_gold_raw):
        result1 = self.func_y_seq(x_in)
        y_pred_raw = self.viterbi_numpy(result1)
        y_gold = np.expand_dims(np.array(y_gold_raw), axis=1)
        y_pred = np.expand_dims(np.array(y_pred_raw), axis=1)
        cost = self.func_validator(x_in, y_pred, y_gold)
        return cost

    def write_model(self, fname, f_indent=False):
        params = self.params
        params_json = {}
        p_keys = ['wb', 'gradients_sq', 'deltas_sq']
        for p_key in p_keys:
            params_json[p_key] = {}
            for w_key in params[p_key]:
                params_json[p_key][w_key] = params[p_key][w_key].get_value().tolist()
        params_json['params'] = {}
        if params.get('params'):
            params_json['params'] = json.dumps(params['params'])
        f = open(fname, "w")
        if f_indent:
            f.write(json.dumps(params_json, indent=4))
        else:
            f.write(json.dumps(params_json))
        f.close()

        # return x_in, y_in, wb, out_result, cost, error_cost, reg_cost,


def main():
    json_item = read_json("example_xy.json")
    config = {
        "s_vocab": 4000,
        "s_embed": 100,
        "s_window": 5,
        "s_hidden": 100,
    }
    mmnk = MaxMgNetwork1(config)

    # result_test = np.random.rand(5, 1, 4)
    # seq1 = mmnk.viterbi_numpy(result_test)

    x_in = np.array(json_item["x"])[:8, :]
    #y_gold = np.expand_dims(np.array(json_item["y"]), axis=1)[:8, :]
    result1 = mmnk.func_y_seq(x_in)
    print 1
    # result2 = mmnk.func_s_seq(x_in)
    # y_pred_raw = mmnk.viterbi_numpy(result1)
    # y_pred = np.expand_dims(np.array(y_pred_raw), axis=1)
    # result3 = mmnk.func_s_seq_two(x_in, y_pred, y_gold)

    #result1 = mmnk.func_y_seq(x_in)
    #y_pred_raw = mmnk.viterbi_numpy(result1)
    #y_pred = np.expand_dims(np.array(y_pred_raw), axis=1)
    #cost = mmnk.func_trainer(x_in, y_pred, y_gold)
    #print cost
    #print y_pred.T
    #print y_gold.T
    #print 1


if __name__ == "__main__":
    main()
