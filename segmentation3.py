import theano.tensor as T
import theano
import numpy as np
from util import read_json, read_file_line, tagDigitEn, get_widx
from collections import OrderedDict
from segeval import segeval

#   U B L I
# U
# B
# L
# I

VALID_TRANS = np.matrix([
    [1, 1, 0, 0],
    [0, 0, 1, 1],
    [1, 1, 0, 0],
    [0, 0, 1, 1],
])


def weight_init_val(w_val):
    return theano.shared(np.array(w_val).astype(theano.config.floatX))


def build_networks(pre_model_params):
    # s_vocab = config["s_vocab"]

    s_embed = np.array(pre_model_params['wb']['w_embed']).shape[1]

    wb = OrderedDict()
    # gdss = {}
    # deltass = {}
    wb['w_embed'] = weight_init_val(pre_model_params['wb']['w_embed'])
    wb['w_hidden'] = weight_init_val(pre_model_params['wb']['w_hidden'])
    wb['b_hidden'] = weight_init_val(pre_model_params['wb']['b_hidden'])
    #wb['w_hidden2'] = weight_init_val(pre_model_params['wb']['w_hidden2'])
    #wb['b_hidden2'] = weight_init_val(pre_model_params['wb']['b_hidden2'])
    #wb['w_hidden3'] = weight_init_val(pre_model_params['wb']['w_hidden3'])
    #wb['b_hidden3'] = weight_init_val(pre_model_params['wb']['b_hidden3'])
    wb['w_out'] = weight_init_val(pre_model_params['wb']['w_out'])
    wb['b_out'] = weight_init_val(pre_model_params['wb']['b_out'])

    x_in = T.imatrix()

    emb_lookup = wb["w_embed"][x_in]
    hidden_input = T.reshape(emb_lookup, newshape=(x_in.shape[0], x_in.shape[1] * s_embed))
    hidden_result = T.tanh(T.dot(hidden_input, wb["w_hidden"]) + wb["b_hidden"])
    #hidden2_result = 0.8 * T.tanh(T.dot(hidden_result, wb["w_hidden2"]) + wb["b_hidden2"])
    #hidden3_result = 0.8 * T.tanh(T.dot(hidden2_result, wb["w_hidden3"]) + wb["b_hidden3"])
    out_result = T.nnet.softmax(T.dot(hidden_result, wb["w_out"]) + wb["b_out"])
    out_result_log = T.log(out_result)

    func = theano.function(inputs=[x_in], outputs=[out_result_log], allow_input_downcast=True)

    return func


#def get_widx(w, word_dict):
#    w_idx = word_dict.get(tagDigitEn(w), None)
#    if w_idx:
#        return w_idx
#    else:
#        return word_dict.get("OOV")


def gen_input_line(line, s_window, pad_id):
    line_word = [pad_id] * (s_window / 2) + line + [pad_id] * (
        s_window / 2)
    case_raw = [line_word[i:i + s_window] for i in range(len(line_word) + 1 - s_window)]
    return case_raw


def viterbi(tag_prob):
    global VALID_TRANS
    max_prob = -np.inf * np.ones(tag_prob.shape)
    max_prob_bt = -1 * np.ones(tag_prob.shape)
    for i in range(tag_prob.shape[0]):
        for j in range(tag_prob.shape[1]):
            if i == 0:
                max_prob[i, j] = tag_prob[i, j]
            else:
                max_prob_temp = -np.inf
                max_prob_id = -1
                for k in range(tag_prob.shape[1]):
                    if VALID_TRANS[k, j] == 1:
                        if max_prob[i - 1, k] >= max_prob_temp:
                            max_prob_temp = max_prob[i - 1, k]
                            max_prob_id = k
                assert max_prob_id != -1
                max_prob[i, j] = tag_prob[i, j] + max_prob_temp
                max_prob_bt[i, j] = max_prob_id
                # max_prob[i,j] = tag_prob[i,j]

    bt_seq = [np.argmax(max_prob[-1, :])]
    for i in range(1, max_prob.shape[0]):
        bt_seq.append(max_prob_bt[-i, bt_seq[-1]])
    bt_seq.reverse()
    #bt_seq = []
    #for i in range(tag_prob.shape[0]):
    #    bt_seq.append(np.argmax(tag_prob[i, :]))
    return bt_seq


def word_segmentation(input_file, output_file, word_dict, network):
    s_window = 5
    pad_id = len(word_dict) - 1
    fout = open(output_file, "w")
    for line in read_file_line(input_file, True):
        line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line.decode("utf-8")])
        line_write = []
        for w in line2:
            w_idx = get_widx(w, word_dict)
            line_write.append(w_idx)
        s = u""
        if len(line2) >= 1:
            input_data = gen_input_line(line_write, s_window, pad_id)
            tag_prob = network(input_data)
            tag_result = viterbi(tag_prob[0])
            for w, t in zip(line2, tag_result):
                s += w
                if t == 0 or t == 2:
                    s += u"  "
        s += u"\n"
        #print s
        fout.write(s.encode('utf-8'))
    fout.close()


def main():
    dict_fname = "word_as_idx_dict.json"
    word_dict = read_json(dict_fname)
    model_fname = "./model/as_model_autoenc_1l_full_1506.json"
    model_params = read_json(model_fname)
    # fpath = "./data/pku_tiny.txt"
    #fin_path = "data/icwb2-data/training/pku_training.utf8"
    #fout_path = "result/pku_train_out.utf8"
    fin_path = "data/icwb2-data/testing/as_test.utf8"
    fout_path = "result/as_test_out.utf8"
    network = build_networks(model_params)
    word_segmentation(fin_path, fout_path, word_dict, network)

    fname_gold = "data/icwb2-data/gold/as_test_gold.utf8"
    fname_result = "result/as_test_out.utf8"
    fname_dict = "data/icwb2-data/gold/as_training_words.utf8"
    segeval(fname_dict, fname_gold, fname_result)

def main2():
    dict_fname = "word_as_idx_dict.json"
    word_dict = read_json(dict_fname)
    model_fname = "./model/as_model_autoenc_1l_full_1055.json"
    model_params = read_json(model_fname)
    network = build_networks(model_params)
    fin_path = "data/mytest/ma.txt"
    fout_path = "data/mytest/ma_out.txt"
    word_segmentation(fin_path, fout_path, word_dict, network)
    fin_path = "data/mytest/xi.txt"
    fout_path = "data/mytest/xi_out.txt"
    word_segmentation(fin_path, fout_path, word_dict, network)


if __name__ == '__main__':
    main()

# f-measure:0.897635288329 62
# f-measure:0.899606375178 67

# f-measure:0.910281882828 40
# f-measure:0.911042752213 47
# f-measure:0.913688436779 48
# f-measure:0.912954144408 52
# f-measure:0.914888684986 55
# f-measure:0.914890326073 59
# f-measure:0.921347666114 2 3
# f-measure:0.922156662886 2 5
# f-measure:0.927625966856 2 17
# f-measure:0.92967078209 2 18
# f-measure:0.930569161912 2 19
# f-measure:0.930398115679 2 20

# f-measure:0.92411415584 35
# f-measure:0.929106677981 48
# f-measure:0.932466521557 55
# f-measure:0.934128338903 1 5
# f-measure:0.935842674495 1 11

# f-measure:0.930490352003 3 34
# f-measure:0.930851598994 3 36
# f-measure:0.933578999391 3 43
# f-measure:0.935308912184 3 47


# f-measure:0.930522211766 4 90
# f-measure:0.93063045773  4 91
# f-measure:0.931585462991 4 100
# f-measure:0.932530594804 4 103
# f-measure:0.933071716072 4 108
# f-measure:0.934204839066 5 3
# f-measure:0.934730481489 5 8
# f-measure:0.935359681629 5 10
# f-measure:0.935901929809 5 14
# f-measure:0.935950095969 5 17
# f-measure:0.936584803413 5 20
# f-measure:0.936761306013 5 22
# f-measure:0.937186086639 5 29
# f-measure:0.937347452578 5 30 best 1
# f-measure:0.937347452578 5 30

# f-measure:0.893429582881 35


# f-measure:0.897766944564 small 23
# f-measure:0.897841168195 small 29
# f-measure:0.897898539466 small 34

# f-measure:0.934292953332 full 771
# f-measure:0.94104988087 full pretrained 1l 290


# f-measure:0.942128512264 as 1055
# f-measure:0.943028218151 as 1252
# f-measure:0.943162556063 as 1506