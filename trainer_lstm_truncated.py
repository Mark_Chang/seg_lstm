import data_generator
import maxmgnet_lstm
import trainer
from segmentation import run_segmentation


def trainer_lstm():
    s_vocab = 4000
    s_window = 5
    batch_size = 20
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 100,
        "s_hidden": 150,
        "batch_size": batch_size,
        "train_file_idx": "./data/converted/pku_train_full_c.utf8",
        "valid_file_idx": "./data/converted/pku_valid_c.utf8",
        "p_optimizer": "clipped_adagrad",
        "p_lr": 0.2,
        "p_eps": 0.,
        "p_mloss": 0.2,
        "p_lambda": 0.0001
    }

    mdata_gen = data_generator.BatchTruncatedDataGenerator(config)
    mmnetwork = maxmgnet_lstm.MaxMgNetworkLSTM(config,mdata_gen)
    #mmnetwork.load_model("model/model_lstm_best_43.json")
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = mtrainer.trainig({
        "print_time": 10,
        "valid_wait_time": 100,
        "model_name": "./model/model_lstm_trunc_%s.json",
        "iter": 500,
        "logfile": "out_lstm_trunc.log"
    })
    run_segmentation(config, model_name, maxmgnet_lstm.MaxMgNetworkLSTM)


if __name__ == "__main__":
    trainer_lstm()
