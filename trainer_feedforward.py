import data_generator
import maxmgnet_feedforward
import trainer
from segmentation import run_segmentation


def trainer_feedforward():
    s_vocab = 4000
    s_window = 5
    batch_size = 1
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 50,
        "s_hidden": 300,
        "batch_size": batch_size,
        "train_file_idx": "./data/converted/pku_train_full_c.utf8",
        "valid_file_idx": "./data/converted/pku_valid_c.utf8",
        "p_lr": 0.1 ,
        "p_eps": 0.0,
        "p_mloss": 0.2,
        "p_optimizer": "clipped_adagrad",
        "p_lambda": 0.0001
    }
    mdata_gen = data_generator.BatchDataGenerator(config)
    mmnetwork = maxmgnet_feedforward.MaxMgNetworkFeedForward(config, mdata_gen)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = mtrainer.trainig({
        "print_time": 10,
        "model_name": "model/model_feedforward_%s.json",
        "iter": 5000,
    })
    run_segmentation(config, model_name, maxmgnet_feedforward.MaxMgNetworkFeedForward)


if __name__ == "__main__":
    trainer_feedforward()
