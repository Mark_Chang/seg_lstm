import numpy as np
def gen_power_seq( len):
    tags = 4
    def gen_power_seq_sub(len, pres, tags):
        if len == 1:
            return pres
        else:
            results = []
            for pre in pres:
                for i in range(tags):
                    results.append(pre + [i])
            return gen_power_seq_sub(len - 1, results, tags)
    return gen_power_seq_sub(len, [[i] for i in range(tags)], tags)

def brute_force_numpy( y_results, WY):
    #y_results = np.reshape(y_results_0, (y_results_0.shape[0], y_results_0.shape[2]))
    seq_tests = gen_power_seq(y_results.shape[0])
    max_seq_id = 0
    max_seq_val = -np.inf
    for sid, seq in enumerate(seq_tests):
        current_val = 0
        for i in range(len(seq)):
            current_val += y_results[i, seq[i]]
            if i > 0:
                current_val += WY[seq[i - 1], seq[i]]
        if current_val > max_seq_val:
            max_seq_id = sid
            max_seq_val = current_val
    return seq_tests[max_seq_id]

