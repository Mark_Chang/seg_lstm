import data_generator
import autoencnet_embedding
import autoencnet_layers
import autoencnet_softmax
import trainer
from segmentation import run_segmentation
from util import read_json



def trainer_autoenc_embed(config):
    config["s_window"] = 11
    mdata_gen = data_generator.SegNegSampDataGenerator(config)
    mmnetwork = autoencnet_embedding.AutoEncNetworkEmbedding(config, mdata_gen)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = ""
    for i, s_negsamp in enumerate([1000, 2000, 3000, config["s_vocab"]]):
        mdata_gen.set_nagsamp_size(s_negsamp)
        model_name = mtrainer.trainig({
            "model_name": "model/as_model_embed_%s_%s.json" % (i, "%s"),
            "print_time": 50,
            "valid_wait_time": 50,
            "iter": 1200,
        })
        mmnetwork.reset_sq()
    return model_name


def trainer_autoenc_layers(config, pre_model_name, layers, network_class):
    config["s_window"] = 5
    mdata_gen = data_generator.SegNegSampDataGenerator(config)
    mdata_gen.set_nagsamp_size(config["s_vocab"])
    mmnetwork = network_class(config, mdata_gen)
    mmnetwork.load_pre_model(pre_model_name)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = mtrainer.trainig({
        "model_name": "model/as_model_autoenc_%s_%s.json" % (layers, "%s"),
        "print_time": 50,
        "valid_wait_time": 50,
        "iter": 1000,
    })
    return model_name


def trainer_autoenc_softmax(config, model_name):
    config["s_window"] = 5
    mdata_gen = data_generator.SeqDataGenerator(config)
    mmnetwork = autoencnet_softmax.AutoEncNetworkSoftmax(config, mdata_gen)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = mtrainer.trainig({
        "model_name": "model/as_model_softmax_full_e_pretrain_%s.json",
        "print_time": 50,
        "valid_wait_time": 30,
        "iter": 50000,
    })


def train_autoenc_all():
    s_vocab = 5000
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_embed": 100,
        "s_e_hidden": 400,
        "s_hidden": 400,
        "s_hidden2": 400,
        "s_hidden3": 400,
        "train_file_idx": "./data/converted/as_train_full_c.utf8",
        "valid_file_idx": "./data/converted/as_valid_c.utf8",
        "p_lr": 0.1,
        "p_eps": 0.0,
        "p_optimizer": "clipped_adagrad",
        "p_lambda": 0.00001
    }
    model_name = trainer_autoenc_embed(config)
    #model_name = "model/model_embed_3_48.json"
    model_name = trainer_autoenc_layers(config, model_name, 1, autoencnet_layers.AutoEncNetwork1Layer)
    model_name = trainer_autoenc_layers(config, model_name, 2, autoencnet_layers.AutoEncNetwork2Layer)
    model_name = trainer_autoenc_layers(config, model_name, 3, autoencnet_layers.AutoEncNetwork3Layer)
    trainer_autoenc_softmax(config, model_name)


if __name__ == "__main__":
    train_autoenc_all()
